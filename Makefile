CXX=g++
CXXFLAGS=-std=c++0x -pedantic -Wall -Wextra

all: dictionary

dictionary: dictionary_cpp.o
	$(CXX) -o $@ $^ $(CXXFLAGS)

clean:
	$(RM) -f *.o dictionary

.PHONY: clean
