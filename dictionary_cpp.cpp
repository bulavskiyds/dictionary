#include <iostream>
#include <vector>
#include <exception>
#include <algorithm>
#include <fstream>
#include <locale>

const std::vector<std::string> keyboard_letters = {
	"qwertyuiop",
	"asdfghjkl",
	"zxcvbnm"
};

class Key {
	char key;
	std::vector<char> neighbors;
public:
	Key(const char c) : key(c) {};
	const char &get_key() const { return key; };
	void set_neighbor(const char c) { neighbors.push_back(c); };
	const std::vector<char> &get_neighbors() const { return neighbors; };
	void sort_neighbors();
};

void Key::sort_neighbors()
{
	if (neighbors.size() > 1)
		sort(neighbors.begin(), neighbors.end(), [](char a, char b) {
			return a < b;
		});
}

class Dictionary {
	std::vector<Key> keys;
	bool check_neighbor(const char key, const char key_next);
public:
	Dictionary(const std::vector<std::string> &latters);
	void lookup_words(std::ifstream &file);
};

Dictionary::Dictionary(const std::vector<std::string> &latters)
{
	size_t latters_size = latters.size();
	if (!latters_size)
		throw std::runtime_error("Bad number of rows");

	for (size_t i = 0; i < latters_size; i++) {
		size_t latters_i_size = latters[i].size();
		if (!latters_i_size)
			throw std::runtime_error("Empty string in a row");

		auto const &latter_prev = (i > 0) ? latters[i - 1] : latters[i];
		auto const &latter_curr = latters[i];
		auto const &latter_next = (i < latters_size - 1) ? latters[i + 1]
				: latters[i];
		for (size_t j = 0; j < latters_i_size; j++) {
			Key key(latter_curr[j]);

			std::locale loc;
			if (!std::islower(latter_curr[j], loc))
				throw std::runtime_error("Wrong symbol in keybord letters");

			if (i > 0) {
				key.set_neighbor(latter_prev[j]);
				if (j < latter_prev.size() - 1)
					key.set_neighbor(latter_prev[j + 1]);
			}

			if (j >= 1)
				key.set_neighbor(latter_curr[j - 1]);
			if (j < latters_i_size - 1)
				key.set_neighbor(latter_curr[j + 1]);

			if (i < latters_size - 1) {
				if (j >= 1)
					key.set_neighbor(latter_next[j - 1]);
				key.set_neighbor(latter_next[j]);
			}
			key.sort_neighbors();
			keys.push_back(key);
		}
	}
	sort(keys.begin(), keys.end(), [](Key &a, Key &b) {
		return a.get_key() < b.get_key();
	});
}

bool Dictionary::check_neighbor(const char key, const char key_next)
{
	auto const &nghbrs = keys[key - 'a'].get_neighbors();
	if (std::binary_search(nghbrs.begin(), nghbrs.end(), key_next))
		 return true;
	return false;
}

void Dictionary::lookup_words(std::ifstream &file)
{
	std::string word_orig;

	while (getline(file, word_orig)) {

		if (word_orig.back() == '\r')
			word_orig.erase(word_orig.size() - 1);

		bool skip_word = false;
		for(auto const &c : word_orig) {
			std::locale loc;
			if (!std::isalpha(c, loc)) {
				skip_word = true;
				break;
			}
		}

		if (skip_word)
			continue;

		if (!word_orig.empty()) {
			bool show_word = true;

			std::string word_copy = word_orig;
			std::transform(word_copy.begin(), word_copy.end(), word_copy.begin(), ::tolower);

			size_t word_copy_size = word_copy.size();
			for (size_t i = 1; i < word_copy_size; i++) {
				if (!check_neighbor(word_copy[i - 1], word_copy[i])) {
					show_word = false;
					break;
				}
			}
			if (show_word)
				std::cout << word_orig << std::endl;
		}
	}
}

int main(int argc, char* argv[])
{
	if (argc != 2) {
		std::cout << "usage: dictionary <file>" << std::endl;
		return -1;
	}

	try {
		std::ifstream file(argv[1]);
		if (!file.good())
			throw std::runtime_error("Failed to open input file");
		Dictionary d(keyboard_letters);
		d.lookup_words(file);
	} catch (std::exception &err) {
		std::cout << err.what();
		return -1;
	}
	return 0;
}
